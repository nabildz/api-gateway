require('dotenv').config();//instatiate environment variables
const Sequelize = require('sequelize');

app          = process.env.APP   || 'development';
port         = process.env.PORT  || '8000';

db_dialect   = process.env.DB_DIALECT    || 'mysql';
db_host      = process.env.DB_HOST       || 'localhost';
db_port      = process.env.DB_PORT       || '3306';
db_name      = process.env.DB_NAME       || 'wahda-db';
db_user      = process.env.DB_USER       || 'root';
db_password  = process.env.DB_PASSWORD   || '';

const connection= new Sequelize(db_name,db_user ,db_password,{
    dialect: 'mysql',
});

module.exports = connection;
