const express 			= require('express');
const router 			= express.Router();
const passport      	= require('passport');

require('./../middlewares/passport')(passport)


//  Microservice controllers

const example                = require("../controllers/example.controller.js");
const util                   = require("../controllers/util.controller.js");
const customer               = require("../controllers/customer.controller.js");
const admin                  = require("../controllers/admin.controller.js");


// Application API routes 

    router.get('/', function(req, res, next) {
        res.json({status:"success", message:"WAHDA API GATEWAY", data:{"version_number":"v1.0.0"}})
    });

    router.get(    '/example/get',           example.get);   
    router.post(   '/example/post',          example.post);   


// Utility API routes 

    router.get(    '/util/branches/all',                util.getBranches);   
    router.get(    '/util/branches/:id',                util.getBranchById);     
    router.get(    '/util/nationalities/all',           util.getNationalities);   
    router.post(   '/util/accounts/check',              util.checkAccount); 


//  Customer API routes
        
    router.post(    '/customer/add',                     customer.addCustomer);   
    router.post(    '/customer/checkticket/',            customer.checkTicket);   
    router.post(    '/customer/verifiy',                 customer.verifiy);   
    router.post(    '/customer/calendar/',               customer.getCalendarByCustomerId);   
    router.post(    '/customer/calendar/slots/',         customer.getSlotsByCustomerId);   
    router.post(    '/customer/book',                    customer.checkAccount); 


//  Admin API routes
    
    // KYC approver
    
        router.get(     '/admin/kyc/kpis',                     admin.kycGetKpis);   
        router.post(    '/admin/kyc/pending/today/',           admin.kycGetPendingToday);   
        router.post(    '/admin/kyc/pending/byDate/',          admin.kycGetPendingByDate);   
        router.post(    '/admin/kyc/view/',                    admin.kycGetById);   
        router.post(    '/admin/kyc/update/',                  admin.kycUpdate);   
        router.post(    '/admin/kyc/approve/',                 admin.kycApprove);   
        router.post(    '/admin/kyc/decline/',                 admin.kycDecline);   



module.exports = router;
